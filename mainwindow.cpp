#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QShortcut>
#include <QFileDialog>
#include <QMessageBox>
#include <QDate>
#include <unistd.h>
#include <libgen.h>
#include <QTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setStyleSheet("background-color: #4A4747;");
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_N), this, SLOT(newFile()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_O), this, SLOT(open()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this, SLOT(save()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::open()
{
    QString newFile = QFileDialog::getOpenFileName(this, tr("Open File"), ".", tr("Text Files (*)"));
    if (newFile == "")
        return;
    else
        file = newFile;

    QFile f(file);
    QTextEdit* textEdit = findChild<QTextEdit*>("textEdit");

    if(!f.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0,"error",f.errorString());
        return;
    }

    textEdit->setText(f.readAll());
    f.close();
}

void MainWindow::save()
{
    if (file == "") {
        file = QFileDialog::getSaveFileName(this, tr("Save File"), ".");
        if (file == "")
            return;
    }

    QFile f(file);
    QTextEdit* textEdit = findChild<QTextEdit*>("textEdit");

    if(!f.open(QIODevice::WriteOnly)) {
        QMessageBox::information(0,"error",f.errorString());
        return;
    }

    f.write(textEdit->toPlainText().toLatin1());
    f.close();
}


void MainWindow::newFile() {
    QTextEdit* textEdit = findChild<QTextEdit*>("textEdit");
    file = "";
    textEdit->setText("");
}
